/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__TIME_MANAGER_H__)
#define __TIME_MANAGER_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_action.h>

#include <amxb/amxb.h>

#include <amxo/amxo.h>
#include <amxo/amxo_save.h>
#include "utils.h"


typedef struct _time_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxp_timer_t* timer;
    bool init_done;
} time_app_t;

int _time_main(int reason,
               amxd_dm_t* dm,
               amxo_parser_t* parser);

void time_init(time_app_t* time_app);
void time_cleanup(void);
void update_time_config_enable(amxd_object_t* time_object);
//the is_initialized and set initialized functions are there to check if the time manager has been synchronized at least once
bool is_initialized(amxd_object_t* time_object);
void set_initialized(amxd_object_t* client_object);
//this function checks the startup routine and will return true after initialization of the plugin
bool time_is_initialized(void);

int set_system_time(const amxc_ts_t* ts);

amxd_dm_t* PRIVATE time_get_dm(void);

amxo_parser_t* PRIVATE time_get_parser(void);

amxd_object_t* PRIVATE time_dm_get_object(const char* dm_object);

amxd_status_t update_time_dm_status_parameter(const char*);

void _print_event(const char* const sig_name,
                  const amxc_var_t* const data,
                  void* const priv);

static inline bool time_dm_get_enable(amxd_object_t* object) {
    return object == NULL ? false : amxd_object_get_value(bool, object, "Enable", NULL);
}
static inline const char* time_dm_get_status(amxd_object_t* object) {
    return object == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(object, "Status"));
}

#ifdef __cplusplus
}
#endif

#endif // __TIME_MANAGER_H__
