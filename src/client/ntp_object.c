/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>

#include "time-manager.h"
#include "client/ntp_object.h"
#include "client/ntp_state.h"
#include "client/time-manager_client.h"
#include "ntp_daemon/service.h"
#include "configuration/time_configuration.h"
#include "time-manager_soc_utils.h"
#include "server/time-manager_server.h"

#include <amxc/amxc.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <malloc.h>

typedef struct {
    uint32_t polling_interval;
    amxp_timer_t* polling_timer;
} ntp_object_state_t;

static ntp_object_state_t instance = {
    .polling_interval = SYNC_NOT_REACHED_POLL_INTERVAL,
    .polling_timer = NULL,
};

void ntp_init(void) {
    amxd_object_t* time = time_dm_get_object("Time");
    amxc_var_t data;
    const char* module = NULL;

    amxc_var_init(&data);
    when_null(time, exit);

    module = time_get_time_ctrl(time);

    mod_time_execute_function("init-daemon", module, &data);
exit:
    amxc_var_clean(&data);
    return;
}

void ntp_cleanup(void) {
    ntp_service_terminate();
}

static void ntp_stop_sync_timer(void) {
    amxp_timer_stop(instance.polling_timer);
}

bool ntp_sync_state(ntp_sync_state_t* state, amxd_object_t* time_object, amxd_object_t* client_object) {
    bool changed = false;
    amxc_var_t data;
    const char* module = NULL;
    uint32_t polling_interval = 0u;
    char* current_status_str = NULL;
    bool seq_mode = false;


    amxc_var_init(&data);
    when_null(time_object, exit);
    when_null(client_object, exit);

    module = time_get_time_ctrl(time_object);
    current_status_str = amxd_object_get_value(cstring_t, client_object, "Status", NULL);
    const ntp_sync_state_t current_state = sync_state_from_string(current_status_str);
    seq_mode = amxd_object_get_value(bool, time_object, "SequentialMode", NULL);


    if(time_dm_get_enable(time_object) && time_dm_get_enable(client_object)) {
        if(ntp_service_running()) {
            *state = mod_time_execute_function("service-state", module, &data);
            if((*state != Synchronized) && !is_initialized(time_object)) {
                *state = Unsynchronized;
            }
            if(*state != Synchronized) {
                uint32_t max_retries = amxd_object_get_value(uint32_t, client_object, "MaxRetries", NULL);
                nm_inc_client_retries(client_object);
                SAH_TRACEZ_WARNING(ME, "Sync attempt: %u", nm_get_client_retries(client_object));
                if((max_retries > 0) && (nm_get_client_retries(client_object) >= max_retries) && !seq_mode) {
                    *state = Error;
                }
            } else {
                nm_reset_client_retries(client_object);
            }
            if((*state == Synchronized) && (current_state != Synchronized)) {
                set_initialized(client_object);
            }
        } else {
            *state = Error;
        }
    } else {
        nm_reset_client_retries(client_object);
        *state = Disabled;
    }

    if(current_state != *state) {
        polling_interval = (Synchronized == *state) ? SYNC_REACHED_POLL_INTERVAL : SYNC_NOT_REACHED_POLL_INTERVAL;
        if(polling_interval != instance.polling_interval) {
            instance.polling_interval = polling_interval;
            ntp_update_sync_timer(polling_interval);
        }
        changed = true;
    }
    free(current_status_str);
exit:
    amxc_var_clean(&data);
    /* If client's interface is down, stop trying to resync */
    if(nm_get_state_client(client_object->priv) == false) {
        ntp_stop_sync_timer();
    }

    return changed;
}

uint32_t ntp_sync_state_polling_interval(void) {
    return instance.polling_interval;
}

void ntp_sync_state_register_timer(amxp_timer_t* timer) {
    instance.polling_timer = timer;
}

void ntp_update_sync_timer(uint32_t start) {
    when_null(instance.polling_timer, exit);
    if(start == 0) {
        start = SYNC_DEFAULT_DELAY;
    }
    amxp_timer_stop(instance.polling_timer);
    amxp_timer_set_interval(instance.polling_timer, instance.polling_interval);
    amxp_timer_start(instance.polling_timer, start);

exit:
    return;
}

bool ntp_service_start(void) {
    bool retval = false;
    amxd_object_t* time = time_dm_get_object("Time");
    amxc_var_t data;
    amxc_var_init(&data);
    when_null(time, exit);
    const char* module = time_get_time_ctrl(time);

    mod_time_execute_function("apply-ntp-config", module, &data);

    when_failed(mod_time_execute_function("service-start", module, &data), exit);
    retval = true;

exit:
    amxc_var_clean(&data);
    return retval;
}

void ntp_service_terminate(void) {
    amxd_object_t* time = time_dm_get_object("Time");
    amxc_var_t data;
    amxc_var_init(&data);
    when_null(time, exit);
    const char* module = time_get_time_ctrl(time);

    mod_time_execute_function("service-terminate", module, &data);

exit:
    amxc_var_clean(&data);
    return;
}
