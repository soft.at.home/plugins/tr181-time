/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include "socket.h"
#include "chronyc.h"

#define ME "chronyc"

static int submit_request(CMD_Request* request, CMD_Reply* reply) {
    int n = 0;
    int sock = unix_socket();
    int ret = -1;

    when_true_trace(sock < 0, exit, ERROR, "Failed to create socket");

    request->pkt_type = PKT_TYPE_CMD_REQUEST;
    request->res1 = 0;
    request->res2 = 0;
    request->pad1 = 0;
    request->pad2 = 0;
    request->sequence = 1;
    request->attempt = htons(1);
    request->version = PROTO_VERSION_NUMBER;

    n = send(sock, request, sizeof(*request), 0);
    when_true_trace(n < 0, exit, ERROR, "Unable to send command to NTP daemon");

    n = wait_for_response(sock);
    when_true_trace(n < 0, exit, ERROR, "Select return: %s", (n == -1 ? "error" : "timeout"));

    n = recv(sock, reply, sizeof(*reply), 0);
    when_true_trace(n < 0, exit, ERROR, "Unable to talk with NTP daemon: (%d) - %s", errno, strerror(errno));

    ret = 0;

exit:
    if(sock >= 0) {
        close(sock);
    }
    return ret;
}

static int request_reply(CMD_Request* request, CMD_Reply* reply, int requested_reply) {
    int status;
    int ret = -1;

    when_failed_trace(submit_request(request, reply), exit, ERROR, "506 Cannot talk to daemon");

    status = ntohs(reply->status);

    if(status != STT_SUCCESS) {
        switch(status) {
        case STT_ACCESSDENIED:
            SAH_TRACEZ_ERROR(ME, "209 Access denied");
            break;
        case STT_FAILED:
            SAH_TRACEZ_ERROR(ME, "500 Failure");
            break;
        case STT_UNAUTH:
            SAH_TRACEZ_ERROR(ME, "501 Not authorised");
            break;
        case STT_INVALID:
            SAH_TRACEZ_ERROR(ME, "502 Invalid command");
            break;
        case STT_BADPKTVERSION:
            SAH_TRACEZ_ERROR(ME, "517 Protocol version mismatch");
            break;
        case STT_BADPKTLENGTH:
            SAH_TRACEZ_ERROR(ME, "518 Packet length mismatch");
            break;
        default:
            SAH_TRACEZ_ERROR(ME, "520 Got unexpected error [%d] from daemon", status);
            goto exit;
        }
    }

    when_false_trace(ntohs(reply->reply) == requested_reply, exit, ERROR, "508 Bad reply from daemon");

    ret = 0;

exit:
    return ret;
}

static int chronyc_cmd_empty_reply(uint16_t cmd, const char* cmd_str) {
    CMD_Request request;
    CMD_Reply reply;
    int ret = -1;

    memset(&reply, 0, sizeof(CMD_Reply));
    memset(&request, 0, sizeof(CMD_Request));

    request.command = htons(cmd);
    when_failed_trace(request_reply(&request, &reply, RPY_NULL), exit, ERROR,
                      "Failed to get %s reply", cmd_str);

    ret = 0;

exit:
    return ret;
}

static int chronyc_cmd_reset(void) {
    return chronyc_cmd_empty_reply(REQ_RESET_SOURCES, "RESET_SOURCES");
}

static int chronyc_cmd_single_source(amxc_var_t* result, uint32_t index) {
    CMD_Request request;
    CMD_Reply reply;
    amxc_var_t* source = NULL;
    char addr[64];
    int ret = -1;

    when_null_trace(result, exit, ERROR, "Invalid argument");

    memset(addr, 0, sizeof(addr));
    memset(&reply, 0, sizeof(CMD_Reply));
    memset(&request, 0, sizeof(CMD_Request));
    request.command = htons(REQ_SOURCE_DATA);
    request.data.source_data.index = htonl(index);
    when_failed_trace(request_reply(&request, &reply, RPY_SOURCE_DATA), exit, ERROR,
                      "Failed to get SOURCE_DATA reply");

    switch(ntohs(reply.data.source_data.ip_addr.family)) {
    case IPADDR_INET4:
        inet_ntop(AF_INET, &reply.data.source_data.ip_addr.addr.in4, addr, sizeof(addr) - 1);
        break;
    case IPADDR_INET6:
        inet_ntop(AF_INET6, &reply.data.source_data.ip_addr.addr.in6, addr, sizeof(addr) - 1);
        break;
    default:
        ret = 0;
        goto exit;
    }

    source = amxc_var_add(amxc_htable_t, result, NULL);
    amxc_var_add_key(cstring_t, source, "name", addr);
    amxc_var_add_key(uint16_t, source, "timeout_counter", ntohs(reply.data.source_data.timeout_counter));

    ret = 0;

exit:
    return ret;
}

int chronyc_cmd_tracking(amxc_var_t* result) {
    CMD_Request request;
    CMD_Reply reply;
    int ret = -1;

    when_null_trace(result, exit, ERROR, "Invalid argument");

    memset(&reply, 0, sizeof(CMD_Reply));
    memset(&request, 0, sizeof(CMD_Request));
    request.command = htons(REQ_TRACKING);

    when_failed_trace(request_reply(&request, &reply, RPY_TRACKING), exit, ERROR,
                      "Failed to get TRACKING reply");

    amxc_var_add_key(uint16_t, result, "leap_status", ntohs(reply.data.tracking.leap_status));

    ret = 0;

exit:
    return ret;
}

int chronyc_cmd_reload(void) {
    return chronyc_cmd_empty_reply(REQ_RELOAD_SOURCES, "RELOAD_SOURCES");
}

int chronyc_cmd_refresh(void) {
    return chronyc_cmd_empty_reply(REQ_REFRESH, "REFRESH");
}

int chronyc_cmd_onoffline(bool onoff) {
    int ret = -1;

    if(onoff) {
        ret = chronyc_cmd_empty_reply(REQ_ONLINE, "ONLINE");
    } else {
        ret = chronyc_cmd_empty_reply(REQ_OFFLINE, "OFFLINE");
        when_failed_trace(ret, exit, ERROR, "Could not set chronyd offline");
        ret = chronyc_cmd_reset();
    }

exit:
    return ret;
}

int chronyc_cmd_nsources(uint32_t* n_sources) {
    CMD_Request request;
    CMD_Reply reply;
    int ret = -1;

    when_null_trace(n_sources, exit, ERROR, "Invalid argument");

    memset(&reply, 0, sizeof(CMD_Reply));
    memset(&request, 0, sizeof(CMD_Request));
    request.command = htons(REQ_N_SOURCES);
    when_failed_trace(request_reply(&request, &reply, RPY_N_SOURCES), exit, ERROR,
                      "Failed to get N_SOURCES reply");

    *n_sources = ntohl(reply.data.n_sources.n_sources);
    ret = 0;

exit:
    return ret;
}

int chronyc_cmd_sources(amxc_var_t* result) {
    uint32_t i = 0;
    uint32_t n_sources = 0;
    int ret = -1;
    amxc_var_t data;

    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_LIST);

    when_null_trace(result, exit, ERROR, "Invalid argument");
    when_failed_trace(chronyc_cmd_nsources(&n_sources), exit, ERROR, "Could not get # of sources");

    for(i = 0; i < n_sources; i++) {
        when_failed_trace(chronyc_cmd_single_source(&data, i), exit, ERROR,
                          "Could not get source number %u", i);
    }

    ret = 0;

exit:
    if(ret == 0) {
        amxc_var_move(result, &data);
    }
    amxc_var_clean(&data);

    return ret;
}
