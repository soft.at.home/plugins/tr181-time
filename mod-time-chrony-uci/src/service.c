/****************************************************************************
**
**SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>

#include "mod_time_chrony.h"
#include "service.h"
#include "chronyc.h"
#include "sequential.h"

#define TERMINATE_TIMEOUT_MS (50)
#define CRASH_THRESHOLD (5u)
#define CLEANUP_INTERVAL (60000)
#define PROCESS_STILL_RUNNING (1)
#define CHRONYD_CONF "/var/etc/chrony.conf"
#define STR_EMPTY(x) (((x) == NULL) || ((x)[0] == '\0'))

typedef struct {
    amxp_subproc_t* proc;
    amxp_slot_fn_t termination_callback;
    uint32_t crash_count;
    amxp_timer_t* health_monitor_timer;
} chrony_proc_t;

static chrony_proc_t chronyd = {
    .proc = NULL,
    .termination_callback = NULL,
    .crash_count = 0u,
    .health_monitor_timer = NULL,
};

static void service_health_monitor(amxp_timer_t* const timer, void* data);
void ntp_daemon_service_refresh(void);

static void crash_handler(const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv);


bool ntp_daemon_service_running(void) {
    return NULL == chronyd.proc ? false : amxp_subproc_is_running(chronyd.proc);
}

bool ntp_daemon_service_start(amxp_slot_fn_t callback) {
    bool rc = false;
    const char* args[] = {"/usr/sbin/chronyd",
        "-n",
        "-f",
        CHRONYD_CONF,
        NULL};

    if((0 != access(CHRONYD_CONF, F_OK))) {
        SAH_TRACEZ_ERROR(ME, "Unable to access chronyd configuration file: %s", CHRONYD_CONF);
        goto exit;
    }

    if(NULL == chronyd.proc) {
        amxp_subproc_new(&chronyd.proc);
        chronyd.termination_callback = callback;
        amxp_slot_connect(amxp_subproc_get_sigmngr(chronyd.proc), "stop", NULL, crash_handler, NULL);
    }

    if(NULL == chronyd.health_monitor_timer) {
        amxp_timer_new(&chronyd.health_monitor_timer, service_health_monitor, NULL);
        amxp_timer_set_interval(chronyd.health_monitor_timer, CLEANUP_INTERVAL);
        if(amxp_timer_running != amxp_timer_get_state(chronyd.health_monitor_timer)) {
            amxp_timer_start(chronyd.health_monitor_timer, CLEANUP_INTERVAL);
        }
    }

    rc = (0 == amxp_subproc_vstart(chronyd.proc, (char**) args));

    sequential_start();

exit:
    return rc;

}

void ntp_daemon_service_terminate(void) {
    sequential_stop();
    if(NULL != chronyd.proc) {
        amxp_slot_disconnect(amxp_subproc_get_sigmngr(chronyd.proc), "stop", crash_handler);
        chronyd.termination_callback = NULL;
        amxp_timer_stop(chronyd.health_monitor_timer);
        amxp_timer_delete(&chronyd.health_monitor_timer);
        chronyd.health_monitor_timer = NULL;

        if(ntp_daemon_service_running()) {
            amxp_subproc_kill(chronyd.proc, SIGTERM);
            if(PROCESS_STILL_RUNNING == amxp_subproc_wait(chronyd.proc, TERMINATE_TIMEOUT_MS)) {
                amxp_subproc_kill(chronyd.proc, SIGKILL);
            }
        }
        amxp_subproc_delete(&chronyd.proc);
    }
    chronyd.crash_count = 0u;
}

void ntp_daemon_service_refresh(void) {
    if(chronyc_cmd_refresh() != 0) {
        SAH_TRACEZ_WARNING(ME, "Failed to refresh chrony daemon");
    }
}

bool ntp_daemon_service_restart(void) {
    bool ret = false;
    ntp_daemon_service_terminate();
    ret = ntp_daemon_service_start(chronyd.termination_callback);

    return ret;
}

static void crash_handler(const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv) {
    int exit_code = amxp_subproc_get_exitstatus(chronyd.proc);
    SAH_TRACEZ_ERROR(ME, "Chronyd unexpectedly terminated. Exit code %d", exit_code);
    sequential_stop();
    if(0 == exit_code) {
        ntp_daemon_service_start(chronyd.termination_callback);
    } else {
        chronyd.crash_count++;
        if(CRASH_THRESHOLD > chronyd.crash_count) {
            amxp_timer_start(chronyd.health_monitor_timer, CLEANUP_INTERVAL);
            ntp_daemon_service_start(chronyd.termination_callback);
        } else {
            SAH_TRACEZ_ERROR(ME, "Allowed crash threshold exceeded. Enter error mode");
            if(NULL != chronyd.termination_callback) {
                chronyd.termination_callback(sig_name, data, priv);
            }
            amxp_timer_stop(chronyd.health_monitor_timer);
        }
    }
}


static void service_health_monitor(UNUSED amxp_timer_t* const timer, UNUSED void* data) {
    if(0u != chronyd.crash_count) {
        SAH_TRACEZ_INFO(ME, "Chrony crash counter cleared");
        chronyd.crash_count = 0u;
    }
}

