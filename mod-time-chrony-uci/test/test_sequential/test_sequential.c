/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "sequential.h"
#include "test_sequential.h"
#include "mock.h"

int test_sequential_setup(UNUSED void** state) {
    sequential_init();
    return 0;
}

int test_sequential_teardown(UNUSED void** state) {
    sequential_exit();
    return 0;
}

void test_01(UNUSED void** state) {
    sequential_stop();

    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_t* Time = NULL;
    Time = amxc_var_add_key(amxc_htable_t, &data, "Time", NULL);
    amxc_var_add_key(bool, Time, "sequential_mode", false);
    amxc_var_add_key(uint32_t, Time, "sequential_mode_timeout", 10000);
    amxc_var_add_key(uint32_t, Time, "ntp_sync_interval", 64);
    amxc_var_add_key(uint32_t, Time, "ntp_retry_interval", 30);

    sequential_configure(&data);
    amxc_var_clean(&data);

    sequential_start();
    /* sequential mode disabled, source shouldn't exit */
    uti_assert_source_doesnt_exist();
}

void test_02(UNUSED void** state) {
    sequential_stop();

    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_t* Time = NULL;
    Time = amxc_var_add_key(amxc_htable_t, &data, "Time", NULL);
    amxc_var_add_key(bool, Time, "sequential_mode", true);
    amxc_var_add_key(uint32_t, Time, "sequential_mode_timeout", 10000);
    amxc_var_add_key(uint32_t, Time, "ntp_sync_interval", 0);
    amxc_var_add_key(uint32_t, Time, "ntp_retry_interval", 30);

    sequential_configure(&data);
    amxc_var_clean(&data);

    sequential_start();
    /* first reload */
    amxut_timer_go_to_future_ms(5000);
    /* sequential mode enabled but there is no client's servers, source shouldn't exit */
    uti_assert_source_doesnt_exist();
}

void test_03(UNUSED void** state) {
    sequential_stop();

    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_t* Time = NULL;
    Time = amxc_var_add_key(amxc_htable_t, &data, "Time", NULL);
    amxc_var_add_key(bool, Time, "sequential_mode", true);
    amxc_var_add_key(uint32_t, Time, "sequential_mode_timeout", 10000);
    amxc_var_add_key(uint32_t, Time, "ntp_sync_interval", 150000);
    amxc_var_add_key(uint32_t, Time, "ntp_retry_interval", 30);

    amxc_var_t* Clients = amxc_var_add_key(amxc_htable_t, &data, "Clients", NULL);
    amxc_var_t* client1 = amxc_var_add_key(amxc_htable_t, Clients, "client1", NULL);
    amxc_var_add_key(bool, client1, "enable", true);
    amxc_var_add_key(bool, client1, "iburst", false);
    amxc_var_t* servers = amxc_var_add_key(amxc_llist_t, client1, "servers", NULL);
    amxc_var_add(cstring_t, servers, "0.server.com");

    sequential_configure(&data);
    amxc_var_clean(&data);

    sequential_start();
    /* first reload */
    amxut_timer_go_to_future_ms(5000);

    uti_assert_source_correct("server 0.server.com minpoll 17 maxpoll 17");
}

void test_04(UNUSED void** state) {
    uint32_t nmbr_of_sources = 1;
    uint16_t timeout = 0;

    sequential_stop();

    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_t* Time = NULL;
    Time = amxc_var_add_key(amxc_htable_t, &data, "Time", NULL);
    amxc_var_add_key(bool, Time, "sequential_mode", true);
    amxc_var_add_key(uint32_t, Time, "sequential_mode_timeout", 10000);
    amxc_var_add_key(uint32_t, Time, "ntp_sync_interval", 64);
    amxc_var_add_key(uint32_t, Time, "ntp_retry_interval", 30);

    amxc_var_t* Clients = amxc_var_add_key(amxc_htable_t, &data, "Clients", NULL);
    amxc_var_t* client1 = amxc_var_add_key(amxc_htable_t, Clients, "client1", NULL);
    amxc_var_add_key(bool, client1, "enable", true);
    amxc_var_add_key(bool, client1, "iburst", true);
    amxc_var_t* servers = amxc_var_add_key(amxc_llist_t, client1, "servers", NULL);
    amxc_var_add(cstring_t, servers, "0.pool.com");
    amxc_var_add(cstring_t, servers, "0.server.com");
    amxc_var_add(cstring_t, servers, "1.server.com");

    sequential_configure(&data);
    amxc_var_clean(&data);

    /* prepare chrony reply: synchronized, source resolved to one ip, no timeout */
    uti_build_chrony_reply(LEAP_Normal, nmbr_of_sources, timeout);
    sequential_start();
    /* first reload */
    amxut_timer_go_to_future_ms(5000);
    uti_assert_source_correct("pool 0.pool.com iburst minpoll 6 maxpoll 6");

    /* check still sync with */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("pool 0.pool.com iburst minpoll 6 maxpoll 6");

    /* simulate a timeout */
    timeout = 1;
    uti_build_chrony_reply(LEAP_Normal, nmbr_of_sources, timeout);
    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    /* not synced anymore due to timeout, check if the next source is in use */
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");

    /* next source is ok, no timeout */
    timeout = 0;
    uti_build_chrony_reply(LEAP_Normal, nmbr_of_sources, timeout);
    /* sync check again */
    amxut_timer_go_to_future_ms(10000);
    /* check if the same source is still in use */
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");

    /* chrony wasn't able to resolve dns, 0 resolved ip */
    nmbr_of_sources = 0;
    uti_build_chrony_reply(LEAP_Normal, nmbr_of_sources, timeout);
    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    /* source should be changed since the previous one couldn't be used */
    uti_assert_source_correct("server 1.server.com iburst minpoll 6 maxpoll 6");
}

void test_05(UNUSED void** state) {
    uint32_t nmbr_of_sources = 1;
    uint16_t timeout = 0;
    sequential_stop();

    amxc_var_t data;
    amxc_var_init(&data);
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);

    amxc_var_t* Time = NULL;
    Time = amxc_var_add_key(amxc_htable_t, &data, "Time", NULL);
    amxc_var_add_key(bool, Time, "sequential_mode", true);
    amxc_var_add_key(uint32_t, Time, "sequential_mode_timeout", 10000);
    amxc_var_add_key(uint32_t, Time, "ntp_sync_interval", 64);
    amxc_var_add_key(uint32_t, Time, "ntp_retry_interval", 30);

    amxc_var_t* Clients = amxc_var_add_key(amxc_htable_t, &data, "Clients", NULL);
    amxc_var_t* client1 = amxc_var_add_key(amxc_htable_t, Clients, "client1", NULL);
    amxc_var_add_key(bool, client1, "enable", true);
    amxc_var_add_key(bool, client1, "iburst", true);
    amxc_var_t* servers = amxc_var_add_key(amxc_llist_t, client1, "servers", NULL);
    amxc_var_add(cstring_t, servers, "0.pool.com");
    amxc_var_add(cstring_t, servers, "0.server.com");
    amxc_var_add(cstring_t, servers, "1.north-america.pool.ntp.org");

    sequential_configure(&data);
    amxc_var_clean(&data);

    /* prepare chrony reply: unsynchronized, for every source */
    uti_build_chrony_reply(LEAP_Unsynchronised, nmbr_of_sources, timeout);

    sequential_start();
    /* first reload */
    amxut_timer_go_to_future_ms(5000);
    uti_assert_source_correct("pool 0.pool.com iburst minpoll 6 maxpoll 6");

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("pool 1.north-america.pool.ntp.org iburst minpoll 6 maxpoll 6");

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_doesnt_exist();

    /* all sources aren't ok, retry with the first source in the list */
    amxut_timer_go_to_future_ms(30000);
    uti_assert_source_correct("pool 0.pool.com iburst minpoll 6 maxpoll 6");

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");


    /* let's say synchronization is ok now*/
    uti_build_chrony_reply(LEAP_Normal, nmbr_of_sources, timeout);

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");

    /* sync check */
    amxut_timer_go_to_future_ms(10000);
    uti_assert_source_correct("server 0.server.com iburst minpoll 6 maxpoll 6");
}
