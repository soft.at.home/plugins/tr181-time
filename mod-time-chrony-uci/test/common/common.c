/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <cmocka.h>
#include <stdlib.h>
#include <stdio.h>


#include <amxc/amxc_macros.h>

#include <amxut/amxut_bus.h>

#include "common.h"

static void mod_time_build_dummy_time_structure(amxc_var_t* data) {
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, data, "enable", true);
    amxc_var_add_key(cstring_t, data, "timezone", "GMT0");
    amxc_var_add_key(uint32_t, data, "clients", 1);
    amxc_var_add_key(uint32_t, data, "servers", 2);
}

static void mod_time_build_dummy_client_structure(amxc_var_t* data) {
    amxc_var_t* client = NULL;
    client = amxc_var_add_key(amxc_htable_t, data, "client1", NULL);

    //todo: add servers list
    amxc_var_add_key(uint32_t, client, "poll", 10);
    amxc_var_add_key(uint32_t, client, "maxpoll", 12);
    amxc_var_add_key(uint32_t, client, "minpoll", 4);
    amxc_var_add_key(bool, client, "iburst", true);
}

static void mod_time_build_dummy_server_structure(amxc_var_t* data) {
    amxc_var_t* server = NULL;
    amxc_var_t* server2 = NULL;
    server = amxc_var_add_key(amxc_htable_t, data, "server1", NULL);
    amxc_var_add_key(bool, server, "enable", 1);
    amxc_var_add_key(cstring_t, server, "interface", "192.168.1.0/24");

    server2 = amxc_var_add_key(amxc_htable_t, data, "server2", NULL);
    amxc_var_add_key(bool, server2, "enable", 1);
    amxc_var_add_key(cstring_t, server2, "interface", "192.168.2.0/24");
}

void time_create_dummy_variant(amxc_var_t* data) {
    when_null(time, exit);
    amxc_var_set_type(data, AMXC_VAR_ID_HTABLE);
    amxc_var_t* time_data;
    amxc_var_t* client_data;
    amxc_var_t* server_data;
    amxc_var_new(&time_data);
    amxc_var_new(&client_data);
    amxc_var_new(&server_data);
    amxc_var_set_type(client_data, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(server_data, AMXC_VAR_ID_HTABLE);
    mod_time_build_dummy_time_structure(time_data);
    mod_time_build_dummy_client_structure(client_data);
    mod_time_build_dummy_server_structure(server_data);
    amxc_var_add_key(amxc_htable_t, data, "Time", amxc_var_constcast(amxc_htable_t, time_data));
    amxc_var_add_key(amxc_htable_t, data, "Clients", amxc_var_constcast(amxc_htable_t, client_data));
    amxc_var_add_key(amxc_htable_t, data, "Servers", amxc_var_constcast(amxc_htable_t, server_data));

    amxc_var_delete(&time_data);
    amxc_var_delete(&client_data);
    amxc_var_delete(&server_data);
exit:
    return;
}

int test_setup(UNUSED void** state) {
    amxut_bus_setup(NULL);

    amxut_bus_handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxut_bus_teardown(NULL);
    return 0;
}

void handle_events(void) {
    amxut_bus_handle_events();
}
